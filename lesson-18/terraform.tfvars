# VARIABLE VALUES

lesson                       = "18"
prefix                       = "delta"
region                       = "us-east-1"
my_ip                        = "90.156.105.168/32"
availability_zones           = ["us-east-1b", "us-east-1c"]
vpc_name                     = "vpc"
vpc_cidr_block               = "10.0.0.0/16"
public_subnet_name           = "public-subnet"
public_subnet_cidrs          = ["10.0.0.0/24", "10.0.1.0/24"]
igw_name                     = "igw"
public_rtb_name              = "public-rtb"
public_sg_name               = "public-sg"
public_server_name           = "public-server"
public_server_eip_name       = "public-server-eip"
ec2_key_name                 = "ec2-public-key"
create_ebs_disk              = false # Controls EBS volume creation
ebs_disk_name                = "ebs-disk"
bucket_name                  = "bucket-ingress-123"
s3_bucket_access_policy_name = "s3-bucket-access-policy"
ec2_iam_role_name            = "ec2-s3-access-role"
ec2_instance_profile_name    = "ec2-instance-profile"



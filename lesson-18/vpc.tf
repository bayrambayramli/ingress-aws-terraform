# VPC
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc

resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr_block
  instance_tenancy     = "default"
  enable_dns_support   = true # DNS resolution within VPC
  enable_dns_hostnames = true # Public DNS hostnames

  tags = {
    Name      = "${var.prefix}-${var.vpc_name}"
    CreatedBy = local.aws_user
  }
}

# PUBLIC SUBNETS
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet

resource "aws_subnet" "public_subnet" {
  count             = length(var.public_subnet_cidrs)
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.public_subnet_cidrs[count.index]
  availability_zone = var.availability_zones[count.index]

  tags = {
    Name      = "${var.prefix}-${var.public_subnet_name}-${var.availability_zones[count.index]}"
    CreatedBy = local.aws_user
  }

  depends_on = [aws_internet_gateway.igw]
}

# INTERNET GATEWAY
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name      = join("-", [var.prefix, var.igw_name])
    CreatedBy = local.aws_user
  }
}


# LATEST AMAZON LINUX IMAGE
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*-hvm-*-x86_64-gp2"]
  }
}

# EC2 KEY PAIR
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair

resource "aws_key_pair" "ec2_key" {
  key_name   = "${var.prefix}-${var.ec2_key_name}"
  public_key = file("${path.module}/keys/ec2_public_key.pem") # Key pair created by "ssh-keygen -b 2048 -t rsa -m PEM -f key_file"

  tags = {
    Name      = "${var.prefix}-${var.ec2_key_name}"
    CreatedBy = local.aws_user
  }
}

# PUBLIC EC2 INSTANCE
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance

resource "aws_instance" "public_server" {
  count                  = length(var.availability_zones)
  availability_zone      = var.availability_zones[count.index]
  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.public_sg.id]
  key_name               = aws_key_pair.ec2_key.key_name
  subnet_id              = aws_subnet.public_subnet[count.index].id
  iam_instance_profile   = aws_iam_instance_profile.ec2_instance_profile.name
  user_data              = templatefile("user-data.tftpl", { efs_id = aws_efs_file_system.efs_disk.id })

  tags = {
    Name      = "${var.prefix}-${var.public_server_name}-${count.index}"
    CreatedBy = local.aws_user
  }

  depends_on = [
    aws_efs_mount_target.efs_disk_mount_target
  ]
}

# ELASTIC IP
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip

resource "aws_eip" "public_server_eip" {
  count      = length(var.availability_zones)
  vpc        = true
  depends_on = [aws_internet_gateway.igw]
  instance   = aws_instance.public_server[count.index].id

  tags = {
    Name      = join("-", [var.prefix, var.public_server_eip_name, count.index])
    CreatedBy = local.aws_user
  }
}

# EC2 INSTANCE PROFILE
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile

resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "${var.prefix}-${var.ec2_instance_profile_name}"
  role = aws_iam_role.ec2_iam_role.name
}


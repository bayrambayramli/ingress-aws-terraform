# IAM POLICY
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy

resource "aws_iam_policy" "s3_bucket_access_policy" {
  name        = "${var.prefix}-${var.s3_bucket_access_policy_name}"
  description = "IAM Policy for S3 Bucket Access"
  policy      = templatefile("s3_bucket_access_policy.tftpl", { bucket_arn = aws_s3_bucket.bucket.arn })

  tags = {
    Name = "${var.prefix}-${var.s3_bucket_access_policy_name}"
  }
}

# IAM ROLE
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role

resource "aws_iam_role" "ec2_iam_role" {
  name                = "${var.prefix}-${var.ec2_iam_role_name}"
  managed_policy_arns = [aws_iam_policy.s3_bucket_access_policy.arn]
  assume_role_policy  = file("assume_role_policy.json")

  tags = {
    Name = "${var.prefix}-${var.ec2_iam_role_name}"
  }
}


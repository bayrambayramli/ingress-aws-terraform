# EBS VOLUME
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ebs_volume

resource "aws_ebs_volume" "ebs_disk" {
  encrypted         = false
  count             = var.create_ebs_disk ? length(var.availability_zones) : 0
  availability_zone = var.availability_zones[count.index]
  size              = 5

  tags = {
    Name = "${var.prefix}-${var.ebs_disk_name}-${count.index}"
  }
}

# EBS VOLUME ATTACHMENT
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/volume_attachment

resource "aws_volume_attachment" "ebs_disk_attach" {
  count                          = var.create_ebs_disk ? length(var.availability_zones) : 0
  device_name                    = "/dev/sdh"
  volume_id                      = aws_ebs_volume.ebs_disk[count.index].id
  instance_id                    = aws_instance.public_server[count.index].id
  stop_instance_before_detaching = true

  depends_on = [
    aws_ebs_volume.ebs_disk
  ]
}

# S3 BUCKET
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket

resource "aws_s3_bucket" "bucket" {
  bucket        = "${var.prefix}-${var.bucket_name}"
  force_destroy = true

  tags = {
    Name = "${var.prefix}-${var.bucket_name}"
  }
}

resource "aws_s3_bucket_acl" "bucket_acl" {
  bucket = aws_s3_bucket.bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_public_access_block" "bucket_public_access" {
  bucket = aws_s3_bucket.bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# ELASTIC FILE SYSTEM

resource "aws_efs_file_system" "efs_disk" {
  encrypted = false

  tags = {
    Name = local.efs_disk_name
  }
}

resource "aws_efs_mount_target" "efs_disk_mount_target" {
  count           = length(var.availability_zones)
  file_system_id  = aws_efs_file_system.efs_disk.id
  subnet_id       = aws_subnet.public_subnet[count.index].id
  security_groups = [aws_security_group.public_sg.id]
}


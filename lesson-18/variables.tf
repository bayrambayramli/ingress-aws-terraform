# VARIABLES

data "aws_caller_identity" "current" {}

locals {
  efs_disk_name = upper(join("-", [var.prefix, "efs-disk"]))
  aws_user      = split("/", data.aws_caller_identity.current.arn)[1]
}

variable "create_ebs_disk" {
  type = bool
}

variable "availability_zones" {
  description = "A list of availability zones"
  type        = list(string)
}

variable "vpc_name" {
  type        = string
  description = "VPC Name"

  validation {
    condition     = length(var.vpc_name) > 2 && substr(var.vpc_name, 0, 3) == "vpc"
    error_message = "The VPC name must start with string 'vpc'"
  }
}

variable "public_subnet_cidrs" {
  type = list(string)
}

variable "lesson" {
  description = "Lesson number"
}

variable "prefix" {
  type        = string
  description = "Prefix for the resource names and Name tags"
}

variable "region" {
  default     = "us-east-1"
  description = "AWS Region"
}

variable "my_ip" {
  description = "My Public IP address"
}

variable "vpc_cidr_block" {}

variable "public_subnet_name" {}

variable "igw_name" {}

variable "public_rtb_name" {}

variable "public_sg_name" {}

variable "ec2_key_name" {}

variable "public_server_name" {}

variable "public_server_eip_name" {}

variable "ebs_disk_name" {}

variable "bucket_name" {}

variable "s3_bucket_access_policy_name" {}

variable "ec2_iam_role_name" {}

variable "ec2_instance_profile_name" {}



# # OUTPUTS

output "public_server_ip_addresses" {
  description = "Public Instance EIP"
  value       = aws_eip.public_server_eip.*.public_ip
}


data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*-hvm-*"]
  }
}

resource "aws_security_group" "ec2_server_sg" {
  name        = var.security_group_name
  description = "Allow traffic from MyIP"
  vpc_id      = var.vpc_id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.security_group_name
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_instance" "ec2_server" {
  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.ec2_server_sg.id]
  key_name               = "ec2-key-pem"
  availability_zone      = var.availability_zone

  tags = {
    Name      = var.instance_name
    CreatedBy = "Terraform"
  }
}

resource "aws_s3_bucket" "delta_s3_bucket" {
  bucket = "delta-s3-bucket-123"
}

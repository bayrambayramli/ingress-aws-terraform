# Common
variable "lesson" {}
variable "aws_account" {}

# S3 Bucket
variable "s3_bucket_source" {}

# Lambda
variable "lambda_function_name" {}

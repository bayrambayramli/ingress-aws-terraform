# S3

locals {
  s3_bucket_resized = "${var.s3_bucket_source}-resized"
}

resource "aws_s3_bucket" "s3_bucket_source" {
  bucket        = var.s3_bucket_source
  force_destroy = true
}

resource "aws_s3_bucket" "s3_bucket_resized" {
  bucket        = local.s3_bucket_resized
  force_destroy = true
}

# Lambda

resource "aws_lambda_function" "create_thumbnail" {
  function_name = var.lambda_function_name
  filename      = "lambda-function/function.zip"
  handler       = "index.handler"
  role          = aws_iam_role.lambda_s3_role.arn

  runtime     = "nodejs18.x"
  memory_size = 512
  timeout     = 10

  depends_on = [
    aws_cloudwatch_log_group.create_thumbnail_logs
  ]
}

# S3 Lambda invocation permission

resource "aws_lambda_permission" "allow_s3" {
  statement_id   = "s3invoke"
  action         = "lambda:InvokeFunction"
  function_name  = aws_lambda_function.create_thumbnail.function_name
  principal      = "s3.amazonaws.com"
  source_arn     = aws_s3_bucket.s3_bucket_source.arn
  source_account = var.aws_account
}

# S3 event notification

resource "aws_s3_bucket_notification" "lambda_trigger" {
  bucket = aws_s3_bucket.s3_bucket_source.id

  lambda_function {
    id                  = "lambda-trigger"
    lambda_function_arn = aws_lambda_function.create_thumbnail.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.allow_s3]
}

# CloudWatch

resource "aws_cloudwatch_log_group" "create_thumbnail_logs" {
  name              = "/aws/lambda/${var.lambda_function_name}"
  retention_in_days = 1
}

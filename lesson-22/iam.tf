# IAM

resource "aws_iam_policy" "lambda_s3_policy" {
  name        = "lambda-s3-policy"
  description = "IAM Policy for S3 Bucket Access"
  policy      = templatefile("lambda_s3_policy.tftpl", { bucket_name = var.s3_bucket_source })
}

resource "aws_iam_role" "lambda_s3_role" {
  name                = "lambda-s3-role"
  managed_policy_arns = [aws_iam_policy.lambda_s3_policy.arn]
  assume_role_policy  = file("assume_role_policy.json")
}


# Common
lesson = "21"
prefix = "lesson-21"
my_ip  = "90.156.105.168/32"

# Security Groups
public_sg_name  = "public-sg"
private_sg_name = "private-sg"

# VPC Module
vpc_name               = "vpc"
vpc_enable_nat_gateway = true
vpc_cidr               = "10.0.0.0/16"
vpc_azs                = ["us-east-1b", "us-east-1c"]
vpc_private_subnets    = ["10.0.1.0/24", "10.0.2.0/24"]
vpc_public_subnets     = ["10.0.11.0/24", "10.0.12.0/24"]

# AutoScaling Module
autoscaling_group_name = "autoscaling-group"
launch_template_name   = "launch-template"
instance_name          = "web-server"
ec2_image_id           = "ami-0b0dcb5067f052a63"
ec2_key_name           = "ec2-key-pem"

# ALB Module
alb_name          = "alb"
target_group_name = "target-group"


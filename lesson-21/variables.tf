# Common
variable "prefix" {}
variable "my_ip" {}
variable "lesson" {}

# Security Groups
variable "public_sg_name" {}
variable "private_sg_name" {}

# VPC Module
variable "vpc_name" {}
variable "vpc_cidr" {}

variable "vpc_azs" {
  type = list(string)
}

variable "vpc_private_subnets" {
  type = list(string)
}

variable "vpc_public_subnets" {
  type = list(string)
}

variable "vpc_enable_nat_gateway" {
  type = bool
}

# AutoScaling Module
variable "autoscaling_group_name" {}
variable "launch_template_name" {}
variable "instance_name" {}
variable "ec2_image_id" {}
variable "ec2_key_name" {}

# ALB Module
variable "alb_name" {}
variable "target_group_name" {}


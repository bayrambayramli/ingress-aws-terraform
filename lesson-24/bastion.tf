
data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*-hvm-*"]
  }
}

resource "aws_instance" "bastion" {
  ami                         = data.aws_ami.amazon_linux.id
  instance_type               = "t2.micro"
  vpc_security_group_ids      = [aws_security_group.public.id]
  key_name                    = var.ec2_key_name
  subnet_id                   = module.vpc.public_subnets[0]
  availability_zone           = module.vpc.azs[0]
  associate_public_ip_address = true

  tags = {
    Name = "${var.prefix}-bastion"
  }
}


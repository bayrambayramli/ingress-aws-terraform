# VPC Module

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.18.1"

  enable_dns_hostnames = true
  enable_dns_support   = true
  name                 = "${var.prefix}-${var.vpc_name}"
  cidr                 = var.vpc_cidr
  azs                  = var.vpc_azs
  private_subnets      = var.vpc_private_subnets
  public_subnets       = var.vpc_public_subnets
  single_nat_gateway   = true
  enable_nat_gateway   = var.vpc_enable_nat_gateway
  instance_tenancy     = "default"
}

# AutoScaling Module

module "autoscaling" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "6.5.3"

  # AutoScaling Group
  name                            = "${var.prefix}-${var.autoscaling_group_name}"
  use_name_prefix                 = false
  min_size                        = 1
  max_size                        = 3
  ignore_desired_capacity_changes = true
  wait_for_capacity_timeout       = 0
  health_check_type               = "ELB" # or "EC2"
  health_check_grace_period       = 300
  vpc_zone_identifier             = module.vpc.private_subnets
  default_cooldown                = 120

  # Launch Template
  launch_template_name            = "${var.prefix}-${var.launch_template_name}"
  launch_template_use_name_prefix = false
  launch_template_description     = "Launch template for EC2 instances"
  update_default_version          = true
  image_id                        = var.ec2_image_id
  instance_type                   = "t2.micro"
  key_name                        = var.ec2_key_name
  enable_monitoring               = true
  security_groups                 = [aws_security_group.private.id]
  user_data                       = filebase64("user-data.txt")
  target_group_arns               = module.alb.target_group_arns

  scaling_policies = {
    cpu-over-50 = {
      policy_type               = "TargetTrackingScaling"
      estimated_instance_warmup = 120
      target_tracking_configuration = {
        predefined_metric_specification = {
          predefined_metric_type = "ASGAverageCPUUtilization"
        }
        target_value = 50
      }
    }
  }

}

# ALB Module

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "8.2.1"

  name               = "${var.prefix}-${var.alb_name}"
  load_balancer_type = "application"
  internal           = false
  vpc_id             = module.vpc.vpc_id
  subnets            = module.vpc.public_subnets
  security_groups    = [aws_security_group.public.id]

  target_groups = [
    {
      name                 = "${var.prefix}-${var.target_group_name}"
      backend_protocol     = "HTTP"
      backend_port         = 80
      target_type          = "instance"
      deregistration_delay = 10

      health_check = {
        enabled             = true
        interval            = 20
        path                = "/index.html"
        port                = "traffic-port"
        healthy_threshold   = 3
        unhealthy_threshold = 3
        timeout             = 5
        protocol            = "HTTP"
      }
    }
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
      action_type        = "forward"
    }
  ]
}

# CloudWatch Module

module "cloudwatch_alarm" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/metric-alarm"
  version = "4.0.0"

  alarm_name          = "CPU-Above-50-Percent"
  alarm_description   = "CPU usage is higher than 50%"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 50
  period              = 60
  namespace           = "AWS/EC2"
  metric_name         = "CPUUtilization"
  statistic           = "Average"

  dimensions = {
    AutoScalingGroupName = module.autoscaling.autoscaling_group_name
  }

  alarm_actions = ["arn:aws:sns:us-east-1:425032532947:CloudWatch-Alarms-SNS-Topic"]
}



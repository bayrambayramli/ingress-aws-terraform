# Common
variable "prefix" {
  type = string
  description = "Prefix for resource names and Name tags"
}

variable "my_ip" {
  type = string
  description = "My public IP address"
}

variable "lesson" {
  type = string
  description = "Lesson number"
}

# Security Groups
variable "public_sg_name" {
  type        = string
  description = "Public security group name"
}

variable "private_sg_name" {
  type        = string
  description = "Private security group name"
}

# VPC Module
variable "vpc_name" {
  type        = string
  description = "VPC name"
}

variable "vpc_cidr" {
  type        = string
  description = "VPC CIDR block"
}

variable "vpc_azs" {
  type        = list(string)
  description = "Availability zones"
}

variable "vpc_private_subnets" {
  type        = list(string)
  description = "Private subnets"
}

variable "vpc_public_subnets" {
  type        = list(string)
  description = "Public subnets"
}

variable "vpc_enable_nat_gateway" {
  type        = bool
  description = "If true, NAT gateway will be created"
}

# AutoScaling Module
variable "autoscaling_group_name" {
  type        = string
  description = "AutoScaling groupn name"
}

variable "launch_template_name" {
  type        = string
  description = "Launch template name"
}

# variable "instance_name" {
#   type        = string
#   description = "EC2 instance name"
# }

variable "ec2_image_id" {
  type        = string
  description = "EC2 image ID"
}

variable "ec2_key_name" {
  type        = string
  description = "EC2 public key name"
}

# ALB Module
variable "alb_name" {
  type        = string
  description = "Application load balancer name"
}

variable "target_group_name" {
  type        = string
  description = "ALB target group name"
}



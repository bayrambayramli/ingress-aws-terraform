# SECURITY GROUP FOR PUBLIC RESOURCES

resource "aws_security_group" "public" {
  name        = join("-", [var.prefix, var.public_sg_name])
  description = "Security Group for public resources"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "SSH from MyIP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  ingress {
    description = "HTTP from MyIP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = join("-", [var.prefix, var.public_sg_name])
  }
}

# SECURITY GROUP FOR PRIVATE RESOURCES

resource "aws_security_group" "private" {
  name        = join("-", [var.prefix, var.private_sg_name])
  description = "Security Group for private resources"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description     = "SSH from public security group"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.public.id]
  }

  ingress {
    description     = "HTTP from public security group"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.public.id]
  }

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = join("-", [var.prefix, var.private_sg_name])
  }
}


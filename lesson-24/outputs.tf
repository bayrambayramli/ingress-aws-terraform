output "alb_dns_name" {
  value       = "http://${module.alb.lb_dns_name}"
  description = "test"
}

output "bastion_public_ip" {
  value       = aws_instance.bastion.public_ip
  description = "test"
}


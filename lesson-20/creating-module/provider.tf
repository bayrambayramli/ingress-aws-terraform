terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.20.0"
    }
  }
  required_version = ">= 1.1.0"
}

provider "aws" {
  region = "us-east-1"

  default_tags {
    tags = {
      Company    = "IngressAcademy"
      CourseName = "AWS-and-Terraform"
      Lesson     = "20"
      ManagedBy  = "Terraform"
    }
  }
}

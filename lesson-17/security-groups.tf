# SECURITY GROUP FOR PUBLIC RESOURCES
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group

resource "aws_security_group" "public_sg" {
  name        = join("-", [var.prefix, var.public_sg_name])
  description = "Security Group for public resources"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description = "SSH from MyIP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = join("-", [var.prefix, var.public_sg_name])
  }
}

# SECURITY GROUP FOR PRIVATE RESOURCES

resource "aws_security_group" "private_sg" {
  name        = join("-", [var.prefix, var.private_sg_name])
  description = "Security Group for private resources"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description     = "SSH from public security group"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.public_sg.id]
  }

  egress {
    description = "Allow all outbound traffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = join("-", [var.prefix, var.private_sg_name])
  }
}


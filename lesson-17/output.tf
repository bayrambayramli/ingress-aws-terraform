# # OUTPUTS

output "public_server_ip" {
  description = "Public Instance IP"
  value       = aws_instance.public_server.public_ip
}

output "private_server_ip" {
  description = "Private Instance IP"
  value       = aws_instance.private_server.private_ip
}

